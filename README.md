### What is this repository for? ###

* This Repository includes the Code for "Passaro Bamboleante".
* This is a project for the course Computer Arquitecture at IST
* Version 0.1

### Divisão de tarefas ###
    
* Passar os diagramas a limpo para o Power Point - 15min @andresousa28
* Função Escreve ecrã final - 1 hora 30 min @andresousa28
* Função Escreve obstáculos - 2 horas
    * Função Gera obstáculo - 30 min
    * Função número aleatório - 1hora 30 min @Ktano
* Função Teste Colisão - 1:30
* Função Escreve Nivel Leds 1 hora  @JerzyPinheiro
* Função Atualiza LCD 1 hora @JerzyPinheiro
* Função atualiza Display 7 Segmentos @JerzyPinheiro
* Estruturar o relatório de 2 páginas - 4 horas @JerzyPinheiro @andresousa28

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact