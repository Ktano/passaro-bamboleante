;Project de Introducao a Arquitetura de computadores
;Grupo 60
;n  56564 Pedro Caetano
;n  82265 Jerzy Pinheiro
;n  84994 Andre Sousa


;Constantes Sistema
SP_INICIAL      EQU     FDFFh
INT_MASK_ADDR   EQU     FFFAh
INT_MASK		EQU		1000000000000011b
JANELA_WRITE	EQU		FFFEh			
JANELA_POS		EQU		FFFCh
ADDR_TEMP		EQU		FFF6h 	;Endereco para escrever quando ocorre a proxima interrupcao em intervalos de 100ms
ADDR_STATE		EQU		FFF7h	;Endereco que define se o temporizador est� ligado ou n�o

;Constantes do Jogo
TEMP_COUNT		EQU		1d		;n� de intervalos de 100ms at� � pr�xima interrupcao

;Constantes para Janela de texto
FIM_TEXTO		EQU		40h
NUM_LINHAS		EQU		24d
NUM_COLUNAS		EQU		80d
PREP_CENTRO		EQU		0C22h	;Linha central a escrever texto e Coluna Central
INIC_CENTRO		EQU		0E1Ch	

;Constantes do Passaro
LINHAS_SALTO	EQU		0200h			;Salta duas linhas
VEL_SALTO		EQU		0100h			;1 linha por 1000ms
POS_V_INICIAL	EQU		0C00h			;Posicao Inicial na coordenada vertival do Passaro 
POS_H			EQU		0014h			;Posicao Horizontal do Passaro
VEL_QUEDA		EQU		FF00h			;Velocidade de queda constante
ACE_GRAV		EQU		FFF0h			;Acelera��o Gravitica
;Constantes dos Obstaculos
ESPACAMENTO		EQU		5d			;Numero de caracteres de espa�amento

;Palavras de Memoria com as variaveis de contagem
				ORIG	8000h
				
IniciaJogo		WORD	0000h
EcraInicio1		STR		'Prepare-se',FIM_TEXTO
EcraInicio2		STR		'Prima o interruptor I1',FIM_TEXTO
EcraFinal		STR		'Fim do Jogo',FIM_TEXTO

PosicaoEsc		WORD	0000h
PosPassaro		WORD	0000h
PosPassaroAux	WORD	0000h
VelPassaro		WORD	0000h
LinhasaSubir	WORD	0000h
VelObstaculo	WORD	0000h			

;Passaro
Passaro			STR		'o>',FIM_TEXTO
LimpaPassaro	STR		'  ',FIM_TEXTO

;Cenario do JOGO
Linha0			STR		'-------------------------------------------------------------------------------',FIM_TEXTO
Linha1			STR		'                                                                               ',FIM_TEXTO
Linha2			STR		'                                                                               ',FIM_TEXTO
Linha3			STR		'                                                                               ',FIM_TEXTO
Linha4			STR		'                                                                               ',FIM_TEXTO
Linha5			STR		'                                                                               ',FIM_TEXTO
Linha6			STR		'                                                                               ',FIM_TEXTO
Linha7			STR		'                                                                               ',FIM_TEXTO
Linha8			STR		'                                                                               ',FIM_TEXTO
Linha9			STR		'                                                                               ',FIM_TEXTO
Linha10			STR		'                                                                               ',FIM_TEXTO
Linha11			STR		'                                                                               ',FIM_TEXTO
Linha12			STR		'                                                                               ',FIM_TEXTO
Linha13			STR		'                                                                               ',FIM_TEXTO
Linha14			STR		'                                                                               ',FIM_TEXTO
Linha15			STR		'                                                                               ',FIM_TEXTO
Linha16			STR		'                                                                               ',FIM_TEXTO
Linha17			STR		'                                                                               ',FIM_TEXTO
Linha18			STR		'                                                                               ',FIM_TEXTO
Linha19			STR		'                                                                               ',FIM_TEXTO
Linha20			STR		'                                                                               ',FIM_TEXTO
Linha21			STR		'                                                                               ',FIM_TEXTO
Linha22			STR		'                                                                               ',FIM_TEXTO
Linha23			STR		'-------------------------------------------------------------------------------',FIM_TEXTO






;Vectors de Interrupcoes
				ORIG 	FE00h
INT1			WORD	SaltaCima
INT2			WORD 	Inicia

				ORIG	FE0Fh
INT15			WORD	CalcPosicao
;Codigo
				ORIG	0000h
				JMP		Inicio
				
;CalcPosicao: Rotina de interrupcao que actualiza pos obstaculos velocidade e passaro
;			a velocidade constante.
;				Entradas:---
;				Saidas: ---
;				Efeitos: altera a velocidade do Passaro para um valor fixo positivo e
;							define o numero de linhas a aumentar
CalcPosicao:	PUSH	R7
				PUSH	R1
				MOV 	R1,M[VelPassaro]
				MOV 	R7,M[PosPassaro]
				MOV 	M[PosPassaroAux],R7
				SUB 	M[PosPassaro],R1	;Adiciona � Posi��o do passaro a velocidade
				CMP 	M[LinhasaSubir],R0
				BR.Z	ApplyGravity
				SUB 	M[LinhasaSubir],R1	;Diminui numero de linhas a subir
				BR.NN	EndTemp
ApplyGravity:	MOV 	M[LinhasaSubir],R0
				MOV 	R1,VEL_QUEDA
				MOV 	M[VelPassaro],R1
				
EndTemp:		MOV 	R7, TEMP_COUNT
				MOV 	M[ADDR_TEMP],R7
				MOV 	R7,1
				MOV 	M[ADDR_STATE],R7
				POP 	R1
				POP 	R7
				RTI
				
				
				
;SaltaCima: Rotina de interrupcao que faz o Passaro Saltar um certo numero de Linhas
;			a velocidade constante.
;				Entradas:---
;				Saidas: ---
;				Efeitos: altera a velocidade do Passaro para um valor fixo positivo e
;							define o numero de linhas a aumentar
SaltaCima:			PUSH 	R1
					MOV 	R1,LINHAS_SALTO
					ADD 	M[LinhasaSubir],R1
					MOV 	R1,VEL_SALTO
					MOV 	M[VelPassaro],R1
					POP 	R1
					RTI

;Inicia: Rotina de interrupt��o que inicia o jogo
;				Entradas:---
;				Saidas: ---
;				Efeitos: altera a velocidade do Passaro para um valor fixo positivo e
;							define o numero de linhas a aumentar
					
					
					
Inicia: 			PUSH	R1
					CMP 	R0,M[IniciaJogo]	;Verifica se o jogo j� come�ou	
					BR.Z	ComecaJogo			;Se ainda n�o come�ou salta para comeca jogo
ComecaJogo: 		MOV 	R1,0001h
					MOV 	M[IniciaJogo],R1
					POP 	R1
					RTI

; EscCar: Rotina que efectua a escrita de um caracter para o ecra.
;       O caracter pode ser visualizado na janela de texto.
;               Entradas: pilha - caracter a escrever | Memoria - Posi��o a escrever
;               Saidas: ---
;                       Efeitos: alteracao do registo R1
;                       alteracao da posicao de memoria M[IO]

EscCar: 		PUSH	R1
				PUSH	R2
				MOV 	R2, M[PosicaoEsc]
				MOV 	M[JANELA_POS],R2
				MOV 	R1, M[SP+4]
				MOV 	M[JANELA_WRITE], R1
				INC 	M[PosicaoEsc]
				POP 	R2
				POP 	R1
				RETN	1
				
; EscString: Rotina que efectua a escrita de uma cadeia de caracter, terminada
;          pelo caracter FIM_TEXTO. Pode-se definir como terminador qualquer
;          caracter ASCII.
;               Entradas: R2 - apontador para o inicio da cadeia de caracteres
;               Saidas: ---
;               Efeitos: ---

EscString:  		PUSH    R1
					PUSH    R2
CicloEsc:   		MOV     R1, M[R2]
					CMP     R1, FIM_TEXTO
					BR.Z    FimEsc
					PUSH    R1
					CALL    EscCar
					INC     R2
					BR      CicloEsc
FimEsc: 			POP     R2
					POP     R1
					RET
					
;IniciaPass: Rotina que inicializa o Passaro e o escreve na janela de texto
;				Entradas: ---
;				Saidas: ---
;				Efeitos: ---
IniciaPass:			PUSH 	R1
					MOV 	R1,POS_V_INICIAL
					MOV 	M[PosPassaro],R1
					MOV 	M[PosPassaroAux],R1
					MOV 	R1,VEL_QUEDA
					MOV 	M[VelPassaro],R1
					MOV 	M[LinhasaSubir],R0

					MOV 	R2,Passaro			;Rotina para escrever o passaro no Ecra
					DSI
					MOV 	R1, M[PosPassaro]
					AND 	R1,FF00h
					ADD 	R1,POS_H
					MOV 	M[PosicaoEsc],R1
					ENI
					CALL	EscString

					POP 	R1
					RET

;EscEspJogo: Rotina que efectua a escrita do espaco de jogo na Janela de Texto
;				Entradas: ---
;				Saidas: ---
;				Efeitos: ---
EscEspJogo:			PUSH	R1
					PUSH	R2
					PUSH	R3
					MOV 	R3,R0
					MOV 	R2,Linha0
					MOV 	R1,NUM_LINHAS
ProximaLinha:		MOV 	M[PosicaoEsc],R3
					Call	EscString
					ADD 	R3,0100h			;Muda para a proxima linha na Janela
					ADD 	R2,NUM_COLUNAS		;Muda para a proxima linha guardada em mem�ria
					DEC 	R1					;Diminui o numero de Linhas a escrever
					BR.NZ	ProximaLinha
					POP 	R3
					POP 	R2
					POP 	R1
					RET
					
;UPDATE: Actualiza o estado do jogo
;				Entradas: ---
;				Saidas: ---
;				Efeitos: ---
UpdateGame:			CALL	UpdatePassaro
					RET
;LigaTemp
LigaTemp:			PUSH	R7
					MOV		R7, TEMP_COUNT
					MOV		M[ADDR_TEMP],R7
					MOV		R7,1
					MOV		M[ADDR_STATE],R7
					POP		R7
					RET

;UpdatePassaro: Actualiza o estado do passaro
;				Entradas: ---
;				Saidas: ---
;				Efeitos: ---

UpdatePassaro:		PUSH	R1
					PUSH	R2
					PUSH	R3
					
					MOV 	R1,FF00h
					MOV 	R3,FF00h
					DSI
					AND 	R1,M[PosPassaroAux]
					AND 	R3,M[PosPassaro]
					CMP 	R1,R3

					BR.Z	FimActualizacao
					MOV 	R2,LimpaPassaro
					ADD 	R1,POS_H
					MOV 	M[PosicaoEsc],R1
					CALL	EscString
					MOV 	R2,Passaro
					ADD 	R3,POS_H
					MOV 	M[PosicaoEsc],R3
					CALL	EscString

FimActualizacao:	ENI
					POP		R3
					POP		R2
					POP		R1
					RET


;Programa Principal
Inicio:				MOV 	R7, SP_INICIAL
					MOV 	SP, R7
					MOV 	R7,INT_MASK
					MOV 	M[INT_MASK_ADDR],R7
					
					MOV 	R7, FFFFh
					MOV 	M[JANELA_POS],R7
					
					
					MOV 	R2, EcraInicio1
					MOV 	R1, PREP_CENTRO
					MOV 	M[PosicaoEsc],R1
					CALL	EscString
					
					MOV 	R1, INIC_CENTRO
					MOV 	M[PosicaoEsc],R1
					MOV 	R2, EcraInicio2
					CALL	EscString
					
					MOV 	M[IniciaJogo],R0
					ENI 
CicloEcraInicio:	CMP 	M[IniciaJogo],R0
					BR.Z 	CicloEcraInicio
					
					
					CALL	EscEspJogo
					CALL	IniciaPass
					
					CAll	LigaTemp
					
CicloJogo:  		CALL	UpdateGame
					BR  	CicloJogo





